# YouTube Crawler
----------------
## Goals
    
  - Crawl videos from YouTube channel/playlist 
  - Retrieve videos details and save them to database 
  - The crawler should crawl periodically and update with new data

### Tech
* Python
* MYSQL
* YouTube APIs 

### Design
 
```sh   

I created four files :
a) Helper Class to define all helper functions that I used in the program
b) Database Class to define all functions that interact with the database
c) Crawler Class to define functions that handling crawl youtube process
d) and finally the main file that controls for running the program on the right way
```

### Decisions

```sh

After the connection with the database done successfully and entered the correct URL,
I did some steps to achieve the goals:

a) I take the id of the channel or the playlist from the URL

b) I called the YouTube APIs for (channel or playlist) with this id and API key that I generated before.
Note: it returns 50 record max per page and it returns nextPageToken key I used this key to get all videos in the channel or the playlist

c) the previous point get all videos ids then I called API to get all details for every video and 
   if you choose the option to download the video, it happens at this point

d)and save some of the details inside the database, if the video id is new I insert a new record or 
   if it is old I update any information that changed like #views.
Note: I used the video id is a primary key to help me use the function of 
( insert on duplicate key update ).

e) finally the program runs periodically to fetch any updated data on the channel or playlist that you inserted first
```

### Installation

Install the dependencies :

Use the package manager pip to install [regex](https://pypi.org/project/regex/).

```sh
$ pip install regex
```
Use the package manager pip to install [importlib](https://pypi.org/project/importlib/).

```sh
$ pip install importlib
```

Use the package manager pip to install [schedule](https://pypi.org/project/schedule).

```sh
$ pip install schedule
```
Upgrade yotube-dl 

```sh
$ pip install --upgrade youtube-dl
```
Use the package manager pip to install [pafy](https://pypi.org/project/pafy/).

```sh
$  pip install pafy
```
Use the package manager pip to install [mysql](https://pypi.org/project/mysql-connector-python/).
```sh
$ pip install mysql-connector-python
```

## Steps to run the app

```sh
1) Change credentials for database connection in file database.py
   > user
   > passwd

2) Run main.py file

3) Insert the channel url or the playlist url 
   channel ex: https://www.youtube.com/channel/UCNeB4M3mZxA7BqMpQT5E1-w/videos
   playlist ex: https://www.youtube.com/watch?v=pzLmESqW3cA&list=PLJ2x-vm-ifqEEEOgAKQB-lKQCcWOSxn4F
   
4) Choose (y) if you want to download the videos

5) Insert number of minutes that program will runs periodically to fetch 
   the new data. 
``` 