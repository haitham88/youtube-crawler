
import json 	
import re
import urllib.request 
import importlib
import os
import schedule	
from crawler import YoutubeCrawler
from helper import Helper
import pafy

mycursor = importlib.import_module('database') 

mycursor.createSchema()

mycursor.createTable()

crawler = YoutubeCrawler()

helper = Helper()

helper.makeDirectory()


url = input("Insert the playlist url or channel url\n")

check = re.findall("list|videos", url)

download_video = input("\n You want to download all videos enter y => yes\n")

num_of_mins = input("\n Insert number of minutes to schedule the periodically running process \n")

helper.run(crawler ,check,url,download_video)

if num_of_mins.isnumeric():
	
	print("The program will run every "+num_of_mins+" minutes to fetch the new data")
	schedule.every(int(num_of_mins)).minutes.do(helper.run, crawler , check,url , download_video)
else: 	
	print("the program will not run periodically because your input was wrong ")

while 1:
	schedule.run_pending()