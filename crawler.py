
import json 	
import re 
import urllib.request 
import importlib
from helper import Helper

mycursor = importlib.import_module('database')

class YoutubeCrawler:
	
	def crawlYoutube(self , type , id , page_token ,download_video):
		helper = Helper()
		api_key = "AIzaSyB0GF3a4bvYFdrRHk4cUO_UWbNZUceIPXY"

		if type == ['list']:
			url = f"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=50&playlistId={id}&key={api_key}&pageToken={page_token}"
		else:
			url = f"https://www.googleapis.com/youtube/v3/search?part=snippet&order=date&maxResults=50&channelId={id}&key={api_key}&pageToken={page_token}"
	
		try:
				json_url = urllib.request.urlopen(url)
				data = json.load(json_url)
				videos = data['items']
				
				for video in videos :
				
					if type == ['list']:
						video_id = video['snippet']['resourceId']['videoId']
					else:
						video_id = video['id']['videoId']
	
					video_details = helper.video_details(video_id,api_key,download_video)

					thum_path = helper.downloadThumbnail(data['items'][0]['snippet']['thumbnails']['high']['url'],video_id)

					original_path = helper.downloadThumbnail(data['items'][0]['snippet']['thumbnails']['default']['url'],video_id)
							
		
					mycursor.insertOrUpdate(video_details['items'][0]['id'],'https://www.youtube.com/watch?v='+video_details['items'][0]['id'],video_details['items'][0]['snippet']['title']
						,video_details['items'][0]['contentDetails']['duration'],video_details['items'][0]['statistics']['viewCount']
						,thum_path[0],video_details['items'][0]['snippet']['thumbnails']['high']['url'],
						original_path[0],video_details['items'][0]['snippet']['thumbnails']['default']['url'])

				if 'nextPageToken' in data:
					self.crawlYoutube(type,id,data['nextPageToken'],download_video)
					return 200
				else:
					return 200
		
		except urllib.error.URLError as e: 
			return 400
		 
	
			