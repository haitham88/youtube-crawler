import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="123456789",
)

mycursor = mydb.cursor()
	
def createSchema():
	try:
		mycursor.execute("CREATE DATABASE youtube")
		print('Database created successfully')
	except :
		print('Schema already exists')
		

def createTable():
	try:
		mycursor.execute("CREATE TABLE youtube.videos (video_id VARCHAR(255) PRIMARY KEY, video_url VARCHAR(255), title VARCHAR(255) , duration VARCHAR(255) ,views INT, thum_path VARCHAR(255) ,thum_url VARCHAR(255),original_img_path VARCHAR(255),original_img_url VARCHAR(255) )")
		print('Table created successfully')

	except :
		print('Table already exists')


def insertOrUpdate(video_id , video_url , title ,duration ,views ,thum_path,thum_url,original_img_path,original_img_url):
	
	mycursor.execute(""" INSERT INTO youtube.videos (video_id, video_url, title,duration ,views ,thum_path,thum_url,original_img_path,original_img_url) VALUES (%s, %s, %s,%s, %s, %s,%s, %s,%s) ON DUPLICATE KEY UPDATE video_url=%s, title=%s,duration=%s , views=%s ,thum_path=%s ,thum_url=%s,original_img_path=%s ,original_img_url=%s """, 
					(video_id , video_url , title ,duration ,views ,thum_path,thum_url,original_img_path,original_img_url, video_url , title ,duration ,views ,thum_path,thum_url,original_img_path,original_img_url))
	mydb.commit()
	print(mycursor.rowcount, "was affected.")

 