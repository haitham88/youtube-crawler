
import json 	
import re
import urllib.request
import os 
import pafy
import schedule

class Helper:
	
	def getIdFromPlaylist(self , url):
		return url.rsplit("list=",1)[1]


	def getIdFromChannel(self , url):
		return url.rsplit("/",2)[1]


	def video_details(self , video_id , api_key , download_video):
		url = f"https://www.googleapis.com/youtube/v3/videos?key={api_key}&id={video_id}&part=snippet,statistics,contentDetails"
		json_url = urllib.request.urlopen(url)
		data = json.load(json_url)		
		if download_video == "y":
			try:
				if not os.path.isfile(os.getcwd()+"\\"+data['items'][0]['snippet']['title']+".mp4"):
					self.downloadVideo(video_id)
			except :
				return data
			
		return data

	def downloadThumbnail(self , image , id ):
		return urllib.request.urlretrieve(image, os.getcwd()+"\\images\\"+id+".jpg")

	def downloadVideo(self , video_id):
		v = pafy.new(video_id)
		videos = v.videostreams
		for video in videos:
			if video.quality in ['640x360','854x480']:
				print("Size is %s" % video.get_filesize())
				filename = video.download()
				print(filename)
				return
	

	def response(self , status):
		if status == 200:
			print("All videos inserted successfully")
		elif status == 400:
			print("No videos")
		else :
			print("You entered a Wrong url")

	def makeDirectory(self):
		try:
			path = os.getcwd()+"\\images"
			os.mkdir(path)	
		except :
			print('folder already exists')

	def run(self ,crawler,check,url,download_video):

		print("start program")
		if check == ['list'] :
			id = self.getIdFromPlaylist(url)
			status = crawler.crawlYoutube(check , id , '' ,download_video)
			self.response(status)

		elif check == ['videos'] :
			id = self.getIdFromChannel(url)
			status = crawler.crawlYoutube(check , id , '' ,download_video)
			self.response(status)

		else:
			self.response(422)		
		
